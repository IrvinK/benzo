class ProfileGrailsPlugin {
    // the plugin version
    def version = "0.1.0"
    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "2.4.4 > *"
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
        "grails-app/views/error.gsp"
    ]

    // TODO Fill in these fields
    def title = "Profile Plugin" // Headline display name of the plugin
    def author = "Selma Cikotic"
    def authorEmail = "secik12@student.sdu.dk"
    def description = '''\
    This plugin provides the functionality of viewing your profile as a logged in employee.
'''

    // URL to the plugin's documentation
    def documentation = "http://grails.org/plugin/profile"

    def developers = [ [ name: "Troels Bay-Petersen", email: "trbay10@student.sdu.dk" ],[ name: "Irvin Kubat", email: "irkub12@student.sdu.dk"]]
}
