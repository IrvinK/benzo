import grails.transaction.Transactional
import org.codehaus.groovy.grails.web.util.WebUtils

@Transactional
class ShowProfileService {
    def show(employee) {
    	
    	def listOfUserInfo = []
    	listOfUserInfo << employee.name
    	listOfUserInfo << employee.address
    	listOfUserInfo << employee.type
    	listOfUserInfo << employee.user.username
    	listOfUserInfo << employee.salaries
    	listOfUserInfo << employee.store.name
    	listOfUserInfo << employee.reservations
    	
    	return listOfUserInfo
    }
}