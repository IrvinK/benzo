<div id = "profilePicture">
				<g:img dir="images" file="default_profile.jpg" style="width:200px"/><br><br>
			</div>

			<label><g:message code="employee.name.label" />: </label>${profileList[0]}<br/>
			<label><g:message code="employee.username" />:</label>${profileList[3]}<br/><br/>
			<label><g:message code="employee.address.label" />:</label>${profileList[1].streetname} </label>${profileList[1].housenumber} <br/>
			<label><g:message/></label>${profileList[1].zipcode} </label>${profileList[1].city}<br/><br/>

			<label><g:message code="employee.type.label" />: </label>${profileList[2]}<br/><br>

			<label><g:message code="employee.salaries.label"/>:</label><br><br>
			<table border="1" style="width:50%">
			<tr>
				<td><label><g:message code="salary.label" /></label></td>
			    <td><label><g:message code="salary.date.label" /></label></td>
			</tr>				
			<g:each in="${profileList[4].sort{it.date}}" var="Salary">	
			  <tr>
			    <td>${Salary.salary}kr.</td>
			    <td><g:formatDate format="dd-MM/yyyy HH:mm" date="${Salary.date}"/></td>	
			  </tr>
			</g:each>
			</table><br>
			
			<label><g:message code="store.label" />: </label>${profileList[5]}<br/><br>

			<label><g:message code="employee.profile.reservations"/>:</label><br><br>
			<table border="1" style="width:23%">
			<tr>
				<td><label><g:message code="reservation.date.label" /></label></td>
			</tr>				
			<g:each in="${profileList[6].sort{it.date}}" var="Reservation">	
			  <tr>
			    <td><g:formatDate format="dd-MM/yyyy HH:mm" date="${Reservation.date}"/></td>		
			  </tr>
			</g:each>
			</table><br>