package bilfirma

class ProfileTagLib {
    static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]
    static encodeAsForTags = [profileButton: [taglib:'none']]

     def profileButton = { attrs ->
        out << render(template:"/temp/profileData", model: [profileList: attrs.profileList], plugin:"profile")
    }
}
