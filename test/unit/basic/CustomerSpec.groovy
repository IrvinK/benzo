package basic

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@TestFor(Customer)
@Mock(Address)
class CustomerSpec extends Specification {

	Customer customer
	Address address

	def setup() {
		address = Mock(Address)
		
		customer = new Customer(name:'Mette', address:address, email:'bikerMice@hotmail.com', password:'123456')
	
		return customer
	}
	
	void "Test valid Customer"() {
		expect:
		customer.validate()
	}
	
	@Unroll('Test #type name on Customer')
	void "Test name on Customer"() {
		given:
		customer.name = name

		expect:
		customer.validate() == result
		
		where:
		name		| result	| type
		'Bobby'		| true		| 'valid'
		' '			| false		| 'blank'
		null		| false		| 'null'
	}

	@Unroll('Test #type email on Customer')
	void "Test email on Customer"() {
		given:
		customer.email = email

		expect:
		customer.validate() == result
		
		where:
		email				| result	| type
		'hej@hotmail.com'	| true		| 'valid'
		' '					| false		| 'blank'
		null				| false		| 'null'
		'hejhotmail.com'	| false		| 'invalid'
	}

	@Unroll('Test #type name on Customer')
	void "Test password on Customer"() {
		given:
		customer.password = password

		expect:
		customer.validate() == result
		
		where:
		password			| result	| type
		'      '			| false		| 'blank'
		null				| false		| 'null'
		'123456'			| true		| 'valid (lower border case)'
		'12345'				| false		| 'invalid (too short)'
		'1234567891234567'	| false		| 'invalid (too long)'
		'123456789123456'	| true		| 'valid (higher border case)'
	}
	
	void "Test toString method"() {
		given:
		customer.name = 'Bob Hansen'
		
		expect:
		customer.toString() == 'Bob Hansen'
	}
}
