package basic

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@TestFor(Employee)
@Mock([Store, Address])
class EmployeeSpec extends Specification {

	Employee employee
	Store store
	Address address

	def setup() {
		store = Mock(Store)
		address = Mock(Address)
		
		employee = new Employee(name:'Mette', address:address, type:EmployeeType.SALESMAN, store:store)
	
		return employee
	}
	
	void "Test valid Employee"() {
		expect:
		employee.validate()
	}
	
	@Unroll("Test #type name on Employee")
	void "Test name on Employee"() {
		given:
		employee.name = name

		expect:
		employee.validate() == result
		
		where:
		name		| result	| type
		'Bobby'		| true		| 'valid'
		' '			| false		| 'blank'
		null		| false		| 'null'
	}
	
	void "Test toString method"() {
		given:
		employee.name = 'Bob Hansen'
		
		expect:
		employee.toString() == 'Bob Hansen'
	}
}
