package basic

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@TestFor(Address)

class AddressSpec extends Specification {
	Address address 

    def setup() {
		address = new Address(streetname:"Spurvevej", housenumber:7, zipcode:5000, city:"Odense")
    }

    void "Test valid address"() {
		expect:
		address.validate()
    }

	@Unroll("Test #type streetname on Address")
	void "Test streetname on Address"() {
		given:
		address.streetname = streetname

		expect:
		address.validate() == result
		
		where:
		streetname	| result	| type
		"Odensevej"	| true		| "valid"
		" "			| false		| "invalid (blank)"
		null		| false		| "invalid (null)"
	}
	
	@Unroll("Test #type housenumber on Address")
	void "Test housenumber on Address"() {
		given:
		address.housenumber = housenumber

		expect:
		address.validate() == result
		
		where:
		housenumber	| result	| type
		0			| false		| "invalid"
		1			| true		| "valid (border case)"
	}

	@Unroll("Test #type zipcode on Address")
	void "Test zipcode on Address"() {
		given:
		address.zipcode = zipcode
		
		expect:
		address.validate() == result
		
		where:
		zipcode		| result	| type
		1000		| true		| "lowest valid"
		9999		| true		| "highest valid"
		999			| false		| "invalid (too low)"
		10000		| false		| "invalid (too high)"
	}
	
	@Unroll("Test #type city on Address")
	void "Test city on Address"() {
		given:
		address.city = city
		
		expect:
		address.validate() == result
		
		where:
		city		| result	| type
		"Odense"	| true		| "valid"
		""			| false		| "invalid (blank)"
		null		| false		| "invalid (null)"
	}
	
}
