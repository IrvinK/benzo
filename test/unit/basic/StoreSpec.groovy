package basic

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@TestFor(Store)
@Mock(Address)
class StoreSpec extends Specification {

	Store store
	Address address 
    def setup() {
    	address = Mock(Address)
		store = new Store(name:"Bobs Biks", address:address, type:StoreType.SALES)
    }

    def cleanup() {
    }

    void "Test valid store"() {
		expect:
		store.validate()
    }
	
	@Unroll("Test #type name on Store")
	void "Test name on Store"() {
		given:
		store.name = name

		expect:
		store.validate() == result
		
		where:
		name		| result	| type
		"Bobs Biks"	| true		| "valid"
		" "			| false		| "invalid (blank)"
		null		| false		| "invalid (null)"
	}
	
	void "Test toString method"() {
		given:
		store.name = "Bobs Biks"
		
		expect:
		store.toString() == "Bobs Biks"
	}
	
	@Unroll("Test StoreType enum (#type)")
	void "Test StoreType enum"() {
		given:
		store.type = storetype
	
		expect:
		store.type.toString() == storetype
		
		where:
		storetype	| type
		"SALES"		| "SALES"
		"REPAIRS"	| "REPAIRS"
	}
}
