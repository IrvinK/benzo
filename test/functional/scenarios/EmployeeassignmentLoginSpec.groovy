package scenarios

import pages.*
import geb.navigator.Navigator
import geb.spock.GebReportingSpec
import spock.lang.Stepwise

/**
 * This is just a demo of some of the possibilities of Geb, and in no way a pretty exampe of how to do it.
 *
 * You should use Pages and Modules, which cleans up the tests significantly
 */
@Stepwise
class EmployeeassignmentLoginSpec extends GebReportingSpec {
    void "Go to Employeeassignment"(){
        when:
        go 'http://localhost:8080/bilfirma/employeeassignment/index'

        then:
        at LoginPage
    }

    void "Invalid login"() {
        when:
        loginf.j_username = "me"
        loginf.j_password = "fefwda"
        signIn.click(LoginPage)

        then:
        at LoginPage
    }


    void "Valid login"() {
        when:
        loginf.j_username = "me"
        loginf.j_password = "password"
        signIn.click(EmployeeassignmentPage)

        then:
        at EmployeeassignmentPage
    }
}

