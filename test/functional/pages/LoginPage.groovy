package pages

import geb.Page

class LoginPage extends Page {

    static url = 'http://localhost:8080/bilfirma/login/auth'
	//static url = 'http://localhost:8080/bilfirma/employeeassignment/index'
    static at = { 
    	title == "Login" 
    }

    static content = {
        loginf { $("form") }
        signIn { $("input", value: "Login") }
    }
}
