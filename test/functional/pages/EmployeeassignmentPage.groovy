package pages

import geb.Page

class EmployeeassignmentPage extends Page {

    //static url = '"http://localhost:8080/bilfirma/"'
    static url = 'http://localhost:8080/bilfirma/employeeassignment/index'
    static at = { 
    	title == "Employeeassignment List"
    }

    static content = {
     	button {$("input",value:"Employeeassignment List")}
    }
}
