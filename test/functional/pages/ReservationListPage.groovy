package pages

import geb.Page

class ReservationListPage extends Page {

    static url = 'http://localhost:8080/bilfirma/employeeassignment/showListOfReservations'
    static at = { 
    	title == "List of reservations" 
    }

    static content = {
        //"showListOfEmployees/${Reservation.id}
        choose{$("a", text: "Assign Employee")}
    }
}
