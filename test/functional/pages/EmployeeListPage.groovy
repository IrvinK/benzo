package pages

import geb.Page

class EmployeeListPage extends Page {

    static url = 'http://localhost:8080/bilfirma/employeeassignment/showListOfEmployees/1'
    static at = { 
    	title == "List of employees" 
    }

    static content = {
   		submitButton { $("input", value: "Assign") }
    }
}
