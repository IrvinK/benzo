package administration

import basic.*
import basic.users.*
import ordering.Reservation
import grails.test.spock.IntegrationSpec
import spock.lang.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
class EmployeeassignmentControllerIntegrationSpec extends IntegrationSpec {

   EmployeeassignmentController controller = new EmployeeassignmentController()

   Reservation reservation   
   Employee salesman
   Employee mechanic
   Address address1
   Address address2
   Car car
   
   def setup() { 
		address1 = new Address(streetname:"street", housenumber:9, zipcode:2000, city:"city")
		address2 = new Address(streetname:"Soluret", housenumber:2, zipcode:2000, city:"odense")
		address1.save()
		address2.save()
		Store store = new Store(name:"hej", address:address1, type:StoreType.SALES, cars:[], reservations:[])
		car = new Car(price:10000000, manufacturer:Manufacturer.BMW)
		car.save()
		store.addToCars(car)
		store.save()

        reservation = new Reservation(store:store, date:new Date(), car:car)
		salesman = new Employee(name:"Svend", type:EmployeeType.SALESMAN, address:address2, store:store)
		mechanic = new Employee(name:"Frank", type:EmployeeType.MECHANIC, address:address2, store:store)

		salesman.save()
		mechanic.save()
		reservation.save()
    }
	
    void "Employeeassignment test"() {
		given:
			controller.params.reservation = reservation.id
			controller.params.employee = salesman.id

		when:
			controller.assign()
			
		then:
			Employeeassignment.count == 1
    }

    void "Test showListEmployee of store type SALES"() {
		given:
			Store store = new Store(name:"Odense", address:address1, type:StoreType.SALES, cars:[], reservations:[])
			store.save()

			store.addToEmployees(salesman)
			store.addToEmployees(mechanic)
			store.addToCars(car)

			def reservation2 = new Reservation(store:store, date:new Date(), car:car)

		when:
			def list = controller.showListOfEmployees(reservation2).employees

		then:
			list.size == 1
			list[0].name == "Svend"
    }

    void "Test showListOfEmployees of store type REPAIRS"() {
		given:
			Store store = new Store(name:"Odense", address:address1, type:StoreType.REPAIRS, cars:[], reservations:[])
			store.save()

			store.addToEmployees(salesman)
			store.addToEmployees(mechanic)
			store.addToCars(car)

			def reservation2 = new Reservation(store:store, date:new Date(), car:car)

		when:
			def list = controller.showListOfEmployees(reservation2).employees

		then:
			list.size == 1
			list[0].name == "Frank"
    }
	
	void "Test showReservationList"() {
		given:
			User admin = User.findByUsername("me")
					
		when:
			def list = controller.showListOfReservations(admin)
		
		then:
			list.size() == Employee.findByUser(admin).store.reservations.size()
	}
}

