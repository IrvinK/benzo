package basic

import grails.test.spock.IntegrationSpec

class CarIntegrationSpec extends IntegrationSpec {
	@Override
	setup() {
	}

	@Override
	cleanup() {
	}

	void "Test valid Car"() {
		given:
		Car car = new Car(price:100000, manufacturer:Manufacturer.FIAT, description:"This is a very nice car")

		expect:
		car.validate()
	}

	void "Test border-case on Car"() {
		given:
		Car car = new Car(price:50000, manufacturer:Manufacturer.FIAT, description:"This is a very nice car")

		expect:
		car.validate()
	}

    void "Test negative price on Car"() {
		given:
		Car car = new Car(price:-20000, manufacturer:Manufacturer.BMW, description:"Cool car")

		expect:
		!car.validate()
	}
}
