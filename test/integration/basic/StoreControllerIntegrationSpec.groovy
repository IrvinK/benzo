package basic

import basic.users.*
import grails.test.spock.IntegrationSpec
import spock.lang.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
class StoreControllerIntegrationSpec extends IntegrationSpec {
	StoreController controller = new StoreController()

	Address address1
	Store store
   
	def setup() {
		address1 = new Address(streetname:"street", housenumber:9, zipcode:2000, city:"city")
		address1.save()
		store = new Store(name:"hej", address:address1, type:StoreType.SALES, cars:[], reservations:[])
		store.save()
	}
	
    void "Test showlistOfAllStores"() {
		given:
			def list = controller.showListOfAllStores()

		expect:
			list.stores.size() == Store.findAll().size()
    }
	
	void "Test createNewStore"() {
		given:
			controller.params.storeType = StoreType.SALES
			controller.params.storeName = "Store"
			controller.params.streetname = "Street"
			controller.params.housenumber = 23
			controller.params.zipcode = 9999
			controller.params.city = "City"
			int storeCount = Store.count
			
		when:
			controller.createNewStore()
			
		then:
			Store.count == storeCount + 1
	}
	
	void "Test createNewAdmin"() {
		given:
			controller.params.name = "John Doe"
			controller.params.streetname = "Street"
			controller.params.housenumber = 23
			controller.params.zipcode = 9999
			controller.params.city = "City"
			controller.params.username = "Admin"
			controller.params.password = "password"
			controller.params.store = store.id
			int userCount = User.count
			int userRoleCount = UserRole.count
			
		when:
			controller.createNewAdmin()
			
		then:
			User.count == userCount + 1
			UserRole.count == userRoleCount + 1
	}
}
