package basic

import grails.test.spock.IntegrationSpec

class EmployeeIntegrationSpec extends IntegrationSpec {
	@Override
	setup() {
	}

	@Override
	cleanup() {
	}
	
	Employee makeValidEmployee() {
		Store store = new Store(type:StoreType.SALES, name:"Shop", streetname:"TEST", zipcode:5000, city:"Odense C" )
		
		store.save()
		
		Employee employee = new Employee(name:"Mette", streetname:"Toftevej", zipcode:5000, city:"Odense C", type:EmployeeType.SALESMAN, store:store)
	
		return employee
	}

	Salary makeSalary() {
		Salary salary = new Salary(date:new Date(), salary:170)
		return salary
	}

	Salary makeLowSalary() {
		Salary salary = new Salary(date:new Date(), salary:100)
		return salary
	}

	Salary makeBorderSalary() {
		Salary salary = new Salary(date:new Date(), salary:105)
		return salary
	}

	void "Test same name on Employee"() {
		given:
		Employee employee1 = makeValidEmployee()
		employee1.name = "Bob"
		Employee employee2 = makeValidEmployee()
		employee2.name = "Bob"
		
		when:
		employee1.save()

		then:
		!employee2.validate()
	}
}
