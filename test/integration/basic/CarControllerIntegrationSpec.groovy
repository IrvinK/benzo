package basic

import basic.users.*
import grails.test.spock.IntegrationSpec
import spock.lang.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
class CarControllerIntegrationSpec extends IntegrationSpec {
	CarController controller = new CarController()

	def setup() {
	}
	
    void "Test showlistOfCars"() {
		given:
			User admin = User.findByUsername("me")
					
		when:
			def list = controller.showListOfCars(admin)
		
		then:
			list.carList.size() == Employee.findByUser(admin).store.cars.size()
    }
	
	void "Test addCar"() {
		given:
			User admin = User.findByUsername("me")
			controller.params.price = 100000
			controller.params.manufacturer = Manufacturer.BMW
			controller.params.description = "En bil"
			int carCount = Employee.findByUser(admin).store.cars.size()
		
		when:
			controller.addCar(admin)
		
		then:
			Employee.findByUser(admin).store.cars.size() == carCount + 1
	}
}
