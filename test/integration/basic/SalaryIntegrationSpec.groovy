package basic

import grails.test.spock.IntegrationSpec

class SalaryIntegrationSpec extends IntegrationSpec {
	@Override
	setup() {
	}

	@Override
	cleanup() {
	}
	
	Employee makeValidEmployee() {
		Store store = new Store(type:StoreType.SALES, name:"Shop", streetname:"TEST", zipcode:5000, city:"Odense C" )
		
		store.save()
		
		Employee employee = new Employee(name:"Mette", streetname:"Toftevej", zipcode:5000, city:"Odense C", type:EmployeeType.SALESMAN, store:store)
	
		return employee
	}
	
	Salary makeValidSalary() {
		Salary salary = new Salary(salary:105, date:new Date(), employee:makeValidEmployee())
		
		return salary
	}

	void "Test valid salary border case"() {
		given:
		Salary salary = makeValidSalary()
		
		expect:
		salary.validate()
	}
	
	void "Test invalid salary"() {
		given:
		Salary salary = makeValidSalary()
		salary.salary = 104
		
		expect:
		!salary.validate()
	}
}
