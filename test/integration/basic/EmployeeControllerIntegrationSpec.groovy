package basic

import basic.users.*
import grails.test.spock.IntegrationSpec
import spock.lang.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
class EmployeeControllerIntegrationSpec extends IntegrationSpec {
	EmployeeController controller = new EmployeeController()
	AddEmployeeService addEmployeeService = new AddEmployeeService()
	
	int employeeCount
	User user
	
	def setup() { 
		employeeCount = Employee.count
   		user = User.findByUsername("me")
    }
	
    void "Add employee test"() {
		given:
		controller.params.name = "Claus"
		controller.params.city = "Odense"
		int i = 9343
		int j = 2
		controller.params.zipcode = i
		controller.params.housenumber = j
		controller.params.streetname = "Street"
		controller.params.username = "Username"
		controller.params.password = "password"
		controller.springSecurityService = [currentUser:user]

		when:
		controller.addEmployee()
			
		then:
		Employee.count == employeeCount + 1
    }
	
	void "Show list of employees test"() {
		given:
			User admin = User.findByUsername("me")
					
		when:
			def list = controller.showListOfEmployees(admin)
		
		then:
			list.employeeList.size() == Employee.findByUser(admin).store.employees.size()
	}
}
