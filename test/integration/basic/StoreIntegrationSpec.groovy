package basic

import grails.test.spock.IntegrationSpec

class StoreIntegrationSpec extends IntegrationSpec {
	@Override
    setup() {
    }

	@Override
    cleanup() {
    }

	Store makeValidStore() {
		Store store = new Store(name:"Bobs Biks", streetname:"Brogade 123", zipcode:5000, city:"Odense C", type:StoreType.SALES)
		
		return store
	}

	void "Test unique name"() {
		given:
		Store store1 = makeValidStore()
		Store store2 = makeValidStore()
		
		when:
		store1.save()
		
		then:
		!store2.validate()
	}
}
