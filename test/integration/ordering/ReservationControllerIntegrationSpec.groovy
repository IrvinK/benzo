package ordering

import grails.test.spock.IntegrationSpec
import spock.lang.*
import basic.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
class ReservationControllerIntegrationSpec extends IntegrationSpec {
	Store store
	Car car
	Address address
	Date date
	ReservereService reservereService = new ReservereService()
	ReservationController controller = new ReservationController()

    def setup() {
		address = new Address(streetname:"street", housenumber:9, zipcode:2000, city:"city")
    	address.save()
    	store = new Store(name:"hej", address:address, cars:[], reservations:[], type:StoreType.SALES)  	
    	car = new Car(price:10000000, manufacturer:Manufacturer.BMW)
    	car.save()
    	store.addToCars(car)
    	store.save()
    	date = new Date()
    	controller.reservereService = reservereService
    	Store.findById(store.id).reservations.count == 1

    	//storeList = []
    	//store2 = newStore(name: "nr.2", address:address, cars:[], reservations:[], type:StoreType.SALES)
    }

	@Override
    def cleanup() {
    }
	
	void "Valid reservation is saved"() {
		given:
			controller.params.store = store.id
			controller.params.car = car.id
			controller.params.date = "01/01/2015"
		when:
			controller.reservere()
		then: 
			Reservation.count == 2
			Store.findById(store.id).reservations.size() == 1
	}
	
	void "Reservation is not saved for null store"() {
		given:
			controller.params.store = null
			controller.params.car = car.id
			controller.params.date = "01/01/2015"
			int resCount = Reservation.count
			
		when:
			controller.reservere()
			
		then:
			Reservation.count == resCount
	}
	
	void "Reservation is not saved for null date input"() {
		given:
			controller.params.store = store.id
			controller.params.car = car.id
			controller.params.date = null
			int resCount = Reservation.count
			
		when:
			controller.reservere()
			
		then:
			Reservation.count == resCount
	}
	
	void "Reservation is not saved for invalid date input"() {
		given:
			controller.params.store = store.id
			controller.params.car = car.id
			controller.params.date = "1234"
			int resCount = Reservation.count
			
		when:
			controller.reservere()
			
		then:
			Reservation.count == resCount
	}
	
	void "Cant create multiple reservations on the same date"() {
		given:
			controller.params.store = store.id
			controller.params.car = car.id
			controller.params.date = "01/01/2015"
			int resCount = Reservation.count
			
		when:
			controller.reservere()
			controller.reservere()
		then: 
			Reservation.count == resCount + 1
	}

	void "Testing findAll() stores"() {
		when:
			def storeCount = controller.showListOfStores().store
		then:
			storeCount.size == 4
	}
}
