package basic

import grails.plugin.i18nEnums.annotations.I18nEnum

@I18nEnum
enum Manufacturer {
	BMW,
	FORD,
	FERRARI,
	OPEL,
	FIAT,
	MERCEDES,
	LAMBORGHINI,
	ASTONMARTIN
}
