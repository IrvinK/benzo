package basic

import grails.plugin.i18nEnums.annotations.I18nEnum

@I18nEnum
enum StoreType {
	SALES,
	REPAIRS
}
