# Group 6

## Project description
In this project we are making a car company that sells and repairs cars.

The application is deployed at:
188.166.62.148

And jenkins can be found at:
188.166.62.148/jenkins

The domain name 'dm844-6.dk' can be used instead of the IP address.

Users available:
Admin: (username: me, password: password)
User: (username: me2, password: password)
Staff: (username: staff, password: password)

## Comments for project 1

* Nice realistic model of constraints
* You haven't tagged your handed in code, and you lack a clear plan for using git
* Is your Jenkins server not running?
* Coding style is extremely shortly described.
* Generally, the report is too the short side
* You do not test all constraints. Fx zipcode is not tested
* You can easily test most of the constraints in unit tests