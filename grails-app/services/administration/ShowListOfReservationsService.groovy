package administration 

import basic.*
import grails.transaction.Transactional

@Transactional
class ShowListOfReservationsService {

	def showReservationList(Employee admin) {
		def reservationList = []
		for (reservation in admin.store.reservations) {
			if (reservation.employee == null) {
				reservationList << reservation
			}
		}
		
		return [reservations:reservationList]
	}
}
