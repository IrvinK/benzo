package administration

import basic.*
import ordering.Reservation
import grails.transaction.Transactional

@Transactional
class AssignService {

    def assignment(Reservation reservation, Employee employee) {
    	Employeeassignment empAss = new Employeeassignment(reservation:reservation, employee:employee)
    	reservation.setEmployee(employee) 
    	empAss.save()
    	reservation.save()
		log.info("Reservation assigned to employee")
    }
}
