package ordering

import basic.Store
import basic.Car
import grails.transaction.Transactional

@Transactional
class ReservereService {	
    def reserve(Store store, Car car, String inputDate) {
		Date date
		
		if (inputDate == null) {
			log.error("Invalid input for reservation")
			return -3
		}
		
		try {
			date = new Date().parse("MM/dd/yyyy", inputDate)
		} catch (java.text.ParseException e) {
			log.warn("Wrong date format for reservation")
			return -1
		}
		
		if (Reservation.countByDateAndCar(date, car) != 0) {
		
			return -2
		}
	
		if (store != null && date != null) {
			Reservation reservation = new Reservation(car:car, date:date, store:store)
			reservation.save()
			store.addToReservations(reservation)
			store.save()
		}
		
		return 0
    }
}
