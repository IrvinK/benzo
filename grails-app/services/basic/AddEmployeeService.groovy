package basic

import basic.users.*
import grails.transaction.Transactional

@Transactional
class AddEmployeeService {
	def springSecurityService
    def addEmpl(user, name, streetname, housenumber,  zipcode, city, username, password) {
		
		Employee admin = Employee.findByUser(user)
		Store store = admin.store
		EmployeeType type
		if (store.type == StoreType.SALES) {
			type = EmployeeType.SALESMAN
		} else {
			type = EmployeeType.MECHANIC
		}
		
		Address address = new Address(streetname:streetname, housenumber:housenumber, zipcode:zipcode, city:city)
		address.save()

		if (address.validate()) {
			def staffRole = Role.findByAuthority('ROLE_STAFF')
			
			User staff = new User(username:username, password:password)
			staff.save(flush:true)
			UserRole.create staff, staffRole, true

			Employee employee = new Employee(name:name, type:type, store:store, address:address, user:staff)			
			employee.save()
			staff.save(flush:true)

			return employee.validate() ? 1 : 0
		}
		
		log.warn("Invalid address given")
		return 0
    }
}
