package basic

import grails.transaction.Transactional

@Transactional
class AddCarService {
	def springSecurityService
    def add(price, manufacturer, description, user) {
		
		Employee admin = Employee.findByUser(user)
		Store store = admin.store	
		Car car = new Car(price:price, manufacturer:manufacturer, description:description)
		car.save()
		if (!car.validate()) {
			log.warn("Invalid car was created")
			return 0
		}
		store.cars << car
		store.save()
		
		log.info("Car was created and added to store")
    }
}
