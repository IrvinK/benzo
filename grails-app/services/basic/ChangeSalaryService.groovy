package basic

import basic.users.*
import grails.transaction.Transactional

@Transactional
class ChangeSalaryService {
	def springSecurityService

    def create(employee, salary) {
		Employee foundEmployee = Employee.findById(employee)
		Salary createdSalary = new Salary(date:new Date(), salary:salary, employee:foundEmployee)
		createdSalary.save()

		if (createdSalary.validate()) {
			foundEmployee.salaries << createdSalary	
			foundEmployee.save()
			log.info("A new salary was added to employee")
			return 1
		}
		
		log.warn("An invalid salary was entered")
		return 0
    }
}
