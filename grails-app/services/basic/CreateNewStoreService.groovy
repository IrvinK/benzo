package basic

import basic.users.*
import grails.transaction.Transactional

@Transactional
class CreateNewStoreService {
    def createStore(storeType, storeName, streetname, housenumber, zipcode, city) {
		
		Address storeAddress = new Address(streetname:streetname, housenumber:housenumber, zipcode:zipcode, city:city)
		storeAddress.save()

		if (!storeAddress.validate()) {
			log.warn("An invalid address was provided")
			return 0
		}
		
		Store store = new Store(type:storeType, name:storeName, address:storeAddress)
		store.save()

		if (!store.validate()) {
			log.warn("An invalid store was provided")
			return 0
		}
		
		log.info("A store was created")
		return store
    }

    def createAdmin(name, streetname, housenumber, zipcode, city, username, password, store) {
		Store storeFound = Store.findById(store)

		EmployeeType employeeType
		if (storeFound.type == StoreType.SALES) {
			employeeType = EmployeeType.SALESMAN
		} else {
			employeeType = EmployeeType.MECHANIC
		}
		
		Address employeeAddress = new Address(streetname:streetname, housenumber:housenumber, zipcode:zipcode, city:city)
		employeeAddress.save()

		if (!employeeAddress.validate()) {
			log.warn("An invalid employee address was provided")
			return 0
		}

		def adminRole = Role.findByAuthority('ROLE_ADMIN')			
		User admin = new User(username:username, password:password)
		admin.save(flush:true)
		UserRole.create admin, adminRole, true

		Employee employee = new Employee(name:name, type:employeeType, store:storeFound, address:employeeAddress, user:admin)			
		employee.save()

		if (!employee.validate()) {
			log.warn("An invalid employee was provided")
			return 0
		}

		storeFound.addToEmployees(employee)
		storeFound.save()

		log.info("An employee was created and added to the store")
		return 1
    }
}
