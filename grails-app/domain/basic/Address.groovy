package basic

class Address {
	String streetname
	Integer housenumber
	Integer zipcode
	String city

	static constraints = {
		streetname blank:false
		housenumber min:1
		zipcode min:1000, max:9999
		city blank:false
	}
}
