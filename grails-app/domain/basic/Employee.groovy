package basic

import ordering.Reservation
import basic.users.User

class Employee {
	String name
	EmployeeType type
	static hasMany = [salaries:Salary, reservations:Reservation]
	Address address
	static hasOne = [store:Store]
	User user

	static constraints = {
		name unique:true, blank:false
		salaries nullable:true
		user nullable:true
		reservations nullable:true
	}
	
	String toString() {
		return name
	}
}
