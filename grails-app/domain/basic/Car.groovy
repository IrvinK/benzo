package basic

class Car {
	Integer price
	Manufacturer manufacturer
	String description

	static constraints = {
		price min:50000
		description nullable:true, blank:true
	}
}
