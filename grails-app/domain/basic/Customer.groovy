package basic

import ordering.Reservation

class Customer {
	String name
	String email
	String password
	Address address
	static hasMany = [reservations:Reservation]

	static constraints = {
		name unique:true, blank:false
		email email:true, blank:false
		password size:6..15, blank:false
		reservations nullable:true
	}
	
	String toString() {
		return name
	}
}
