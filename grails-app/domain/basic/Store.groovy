package basic

import ordering.Reservation

class Store {
	StoreType type
	String name
	Address address
	static hasMany = [employees:Employee, cars:Car, reservations:Reservation]

	static constraints = {
		name unique:true, blank:false
	}
	
	String toString() {
		return name
	}
}
