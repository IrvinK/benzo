package basic

class Salary {
	Date date
	Integer salary
	static belongsTo = [employee:Employee]

	static constraints = {
		salary min:105
	}
	
	String toString() {
		return date?.format('dd/MM/yyyy') + ': ' + salary + ';-'
	}
}
