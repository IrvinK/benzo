package ordering

import basic.Store
import basic.Car
import basic.Employee

class Reservation {
	Date date
	static hasOne = [store:Store]
	Car car
	Employee employee

	static constraints = {
		car nullable:true
		employee nullable:true
	}

	void setEmployee(Employee e) {
		employee = e 
	}
	
	String toString() {
		return 'Reservation ID: ' + id
	}
}
