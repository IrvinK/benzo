import basic.Car
import basic.Address
import basic.Store
import basic.StoreType
import basic.Manufacturer
import basic.Employee
import basic.EmployeeType
import basic.Customer
import ordering.Reservation

import basic.users.*

import grails.util.Environment

class BootStrap {
    def init = { servletContext ->
		if (Environment.current == Environment.PRODUCTION){
			if (User.count() == 0){
				UserRole.executeUpdate('delete from UserRole')
				User.executeUpdate('delete from User')
				Role.executeUpdate('delete from Role')
				
				//Create user roles and users
				def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
				def userRole = new Role(authority: 'ROLE_USER').save(flush: true)
				def staffRole = new Role(authority: 'ROLE_STAFF').save(flush: true)

				def testUser1 = new User(username: 'me', password: 'password')
				def testUser2 = new User(username: 'me2', password: 'password')
				def testUser3 = new User(username: 'staff', password: 'password')

				testUser1.save(flush: true)
				testUser2.save(flush: true)
				testUser3.save(flush: true)

				UserRole.create testUser1, adminRole, true
				UserRole.create testUser2, userRole, true
				UserRole.create testUser3, staffRole, true
				
				def address4 = new Address(streetname: "Spurvevej", housenumber: 5, zipcode: 5230, city: "Odense M")
				address4.save(flush: true)
				
				def store1 = new Store(type: StoreType.SALES, name: "Bennys Biler", address: address1).save(flush: true).save(flush: true)
				store1.save(flush: true)
				
				def admin = new Employee(name: "Jacob Hansen", type:EmployeeType.MANAGEMENT, address:address4, store:[store1], user:testUser1)
				def staff = new Employee(name: "Marco Flanderson", type:EmployeeType.SALESMAN, address:address4, store:[store1], user:testUser3)
				def user = new Customer(name: "Marie Jensen", email:"maria@hotmail.com",password:"secret",address:address4)
				
				admin.save(flush: true)
				user.save(flush: true)
				staff.save(flush: true)
				
				assert User.count() == 3
				assert Role.count() == 3
				assert UserRole.count() == 3
			}
		} else {
			//Create user roles and users
			def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
			def userRole = new Role(authority: 'ROLE_USER').save(flush: true)
			def staffRole = new Role(authority: 'ROLE_STAFF').save(flush: true)

			def testUser1 = new User(username: 'me', password: 'password')
			def testUser2 = new User(username: 'me2', password: 'password')
			def testUser3 = new User(username: 'staff', password: 'password')

			testUser1.save(flush: true)
			testUser2.save(flush: true)
			testUser3.save(flush: true)

			UserRole.create testUser1, adminRole, true
			UserRole.create testUser2, userRole, true
			UserRole.create testUser3, staffRole, true

			assert User.count() == 3
			assert Role.count() == 3
			assert UserRole.count() == 3
			/*--*/
			
			//Create some data
			def car1 = new Car(price: 150000, manufacturer: Manufacturer.MERCEDES, description: "Testbil #1")
			def car2 = new Car(price: 175000, manufacturer: Manufacturer.BMW, description: "Testbil #2")
			def car3 = new Car(price: 200000, manufacturer: Manufacturer.FORD, description: "Testbil #3")
			
			car1.save(flush: true)
			car2.save(flush: true)
			car3.save(flush: true)
			
			def address1 = new Address(streetname: "Brogade", housenumber: 21, zipcode: 5000, city: "Odense C")
			def address2 = new Address(streetname: "Bagergade", housenumber: 7, zipcode: 5230, city: "Odense M")
			def address3 = new Address(streetname: "Campusvej", housenumber: 55, zipcode: 5230, city: "Odense M")
			def address4 = new Address(streetname: "Spurvevej", housenumber: 5, zipcode: 5230, city: "Odense M")
			
			address1.save(flush: true)
			address2.save(flush: true)
			address3.save(flush: true)
			address4.save(flush: true)
			
			def store1 = new Store(type: StoreType.SALES, name: "Bennys Biler", address: address1)
			def store2 = new Store(type: StoreType.SALES, name: "Biler XXL", address: address2)
			def store3 = new Store(type: StoreType.REPAIRS, name: "Motorolie inc.", address: address3)
			
			store1.addToCars(car1)	
			store2.addToCars(car2)
			store2.addToCars(car3)
			
			store1.save(flush: true)
			store2.save(flush: true)
			store3.save(flush: true)
					
			def admin = new Employee(name: "Jacob Hansen", type:EmployeeType.MANAGEMENT, address:address4, store:[store1], user:testUser1)
			def staff = new Employee(name: "Marco Flanderson", type:EmployeeType.SALESMAN, address:address4, store:[store1], user:testUser3)
			def user = new Customer(name: "Marie Jensen", email:"maria@hotmail.com",password:"secret",address:address4)
			
			admin.save(flush: true)
			user.save(flush: true)
			staff.save(flush: true)

			def reservation = new Reservation(date:new Date(),store:store1,car:car1)

			reservation.save(flush: true)

			store1.addToEmployees(admin)
			store1.addToEmployees(staff)
			store1.addToReservations(reservation)

			store1.save(flush: true)

			assert Car.count() == 3
			assert Address.count() == 4
			assert Store.count() == 3
			assert Employee.count == 2
			assert Customer.count == 1
			assert Reservation.count == 1
		}
    }
	
    def destroy = {
    }
}
