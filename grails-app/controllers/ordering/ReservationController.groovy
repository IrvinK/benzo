package ordering

import basic.Car
import basic.Store
import grails.plugin.springsecurity.annotation.Secured

class ReservationController {
	static scaffold = true
	def springSecurityService
	def messageSource

	ReservereService reservereService
	@Secured(['ROLE_USER'])	
	def reservere() {
		Store store = Store.findById(params.store)
		Car car = Car.findById(params.car)
		
		String date = params.date
		
		int i = reservereService.reserve(store, car, date)
		
		if (i == -1) {
			flash.error = message(code:"reservation.error.invalid_date")
		} else if (i == -2) {
			flash.error = message(code:"reservation.error.occupied_date")
		} else if (i == -3) {
			flash.error = message(code:"reservation.error.invalid_input")
		}
	}
	
	//show list of stores
	@Secured(['ROLE_USER'])
	def showListOfStores() {
		def store = Store.findAll()
		return [store:store]
	}

	@Secured(['ROLE_USER'])
	def showListOfCars(Store store) {
		return [cars:store?.cars, store:store]
	}
	
	@Secured(['ROLE_USER'])
	def showDates() {
		return
	}
}
