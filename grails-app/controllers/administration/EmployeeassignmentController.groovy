package administration

import basic.Employee
import basic.Store
import basic.users.User
import ordering.Reservation
import basic.EmployeeType
import basic.StoreType

import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN', 'ROLE_STAFF'])
class EmployeeassignmentController {
	AssignService assignService
	def springSecurityService
	ShowListOfReservationsService showListOfReservationsService

	@Secured(['ROLE_ADMIN'])
	def index() {
		return [employeeassignmentInstanceList:Employeeassignment.findAll()]
	}
	
	@Secured(['ROLE_ADMIN'])
	def showListOfReservations(User inputUser) {
		def user = springSecurityService.currentUser
		
		if (user == null) {
			user = inputUser
		}
		
		Employee admin = Employee.findByUser(User.findById(user.id))
		return showListOfReservationsService.showReservationList(admin)
	}

	@Secured(['ROLE_ADMIN'])
 	def showListOfEmployees(Reservation reservation) {
		def employeeList = []
		
		Closure makeList = { EmployeeType et -> 
						for (employee in Store.findById(reservation.store.id).employees) {
							if (employee.type == et) {
							   employeeList << employee
							}
						}
					}

		if (reservation.store.type == StoreType.REPAIRS) {
			makeList(EmployeeType.MECHANIC)
		} else if (reservation.store.type == StoreType.SALES) {
			makeList(EmployeeType.SALESMAN)
		}

		return [employees:employeeList]
	}
	@Secured(['ROLE_ADMIN'])
	def assign() {
		Reservation reservation = Reservation.findById(params.reservation)
		Employee employee = Employee.findById(params.employee)	
		assignService.assignment(reservation, employee)
	}
}
