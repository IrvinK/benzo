package basic

import basic.users.*
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
class CarController {
	static scaffold = true

	def springSecurityService
	def addCarService
	
	def showListOfCars(User inputUser) {
		def user = springSecurityService.currentUser
		
		if (user == null) {
			user = inputUser
		}
		
		Employee admin = Employee.findByUser(user)

		def carList = admin.store.cars
		return [carList:carList]
	}

	def addCar(User inputUser) {
		def user = springSecurityService.currentUser 

		if (user == null) {
			user = inputUser
		}
		
		def i = addCarService.add(params.price, params.manufacturer, params.description, user)
		
		if (i == 0) {
			flash.error = "error"
		}
	}
}
