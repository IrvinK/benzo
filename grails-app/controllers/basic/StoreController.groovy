package basic

import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
class StoreController {
	CreateNewStoreService createNewStoreService

    static scaffold = true

    def showListOfAllStores() {
        return [stores:Store.findAll()]
    }

    def createNewStore() {
        assert params.storeType != null

    	def i = createNewStoreService.createStore(params.storeType, params.storeName, params.streetname, params.housenumber, params.zipcode, params.city)
        
        if (i == 0) {
            flash.error = "error"
        } else {
            return [store:i.id]
        }   
    }

    def createNewAdmin() {
        assert params.store != null
        int i = createNewStoreService.createAdmin(params.name, params.streetname, params.housenumber, params.zipcode, params.city, params.username, params.password, params.store)
        
		if (i == 0) {
            flash.error = "error"
        } else {
            flash.message = "good"
        }
    }
}
