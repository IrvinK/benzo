package basic

import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
class SalaryController {
	static scaffold = true
}
