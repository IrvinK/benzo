package basic

import basic.users.*
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
class EmployeeController {
	AddEmployeeService addEmployeeService
	ChangeSalaryService changeSalaryService
	def showProfileService

	static scaffold = true
	
	def springSecurityService

	@Secured(['ROLE_ADMIN'])
	def addEmployee() {
		def user = springSecurityService.currentUser
		
		int i = addEmployeeService.addEmpl(user, params.name[0], params.streetname, params.housenumber, params.zipcode, params.city, params.username[0], params.password[0])
		if (i==0){
			flash.error="error"
		}else{
			flash.message="good"
		}
	}

	@Secured(['ROLE_ADMIN'])
	def showListOfEmployees(User inputUser) {
		def user = springSecurityService.currentUser

		if (user == null) {
			user = inputUser
		}
		
		Employee admin = Employee.findByUser(user)

		def employeeList = admin.store.employees
		return [employeeList:employeeList]
	}

	@Secured(['ROLE_ADMIN'])
	def changeSalary(){

		int i = changeSalaryService.create(params.employee, params.salary)

		if (i == 0) {
			flash.error = "error"
		} else {
			flash.message = "good"
		}
	}
	
@Secured(['ROLE_ADMIN','ROLE_STAFF'])
	def showProfile(){
		def user = springSecurityService.currentUser 
		Employee employee = Employee.findByUser(user)

		def infoList = showProfileService.show(employee)

		return [profileList: infoList]
	}
}
