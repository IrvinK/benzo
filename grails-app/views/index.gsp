<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="title.welcome" /></title>
	</head>
	<body>
		<div id="page-body" role="main">
			<h1><g:message code="title.welcome" /></h1>
			<div id="modules">
				<ul>
					<div id="homepageImage">
						<ul> 
							<g:img dir="images" file="homepage.jpg" style="width:550px"/>
					
						</ul>
					</div>
					<li><g:link resource="basic"><g:message code="homepage.administration.label" /></g:link></li>
					<li><g:link controller="reservation" action="showListOfStores"><g:message code="store.reservations.label" /></g:link></li>
					<li><g:link controller="employeeassignment" action="index"><g:message code="homepage.employeeAssignment.label" /></g:link></li><br><br>
					
					<div id='homepageText'>
        				<g:message code="homepage.description.part1"/></br></br>
        				<g:message code="homepage.description.part2"/></br></br>
        			</div>

				</ul>

			</div>
			
		</div>
	</body>
</html>
