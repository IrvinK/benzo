<%@ page import="ordering.Reservation" %>
<%@ page import="basic.StoreType" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="reservation">
		<title><g:message code="store.choose_store.label"/></title>
	</head>
	<body>
		<h1><g:message code="store.choose_store.label" /></h1>
		<g:each in="${store}" var="Store">
			<article class="reservation">
				<label><g:message code="store.label" />:</label>${Store.name}<br/>
				<label><g:message code="store.type.label" />:</label><g:message code="dk.StoreType.${Store.type}"/><br/>
				<label><g:message code="store.address.label" />:</label>${Store.address.streetname} ${Store.address.housenumber}<br/>
				
				<g:if test="${Store.type == StoreType.SALES}">
					<a href="showListOfCars/${Store.id}"><g:message code="store.choose_store_link"/></a>
				</g:if>
				<g:else>
					<a href="showDates/${Store.id}"><g:message code="store.choose_store_link"/></a>
				</g:else>
			</article>
		</g:each>
	</body>
</html>
