<%@ page import="ordering.Reservation" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="reservation">
		<title><g:message code="store.choose_car.label"/></title>
	</head>
	<body>
		<h1><g:message code="store.choose_car.label" /></h1>
		<g:each in="${cars}" var="Car">
			<article class="store">
				<g:form action="reservere">
					<g:hiddenField name="store" value="${params?.id}"/>
					<g:hiddenField name="car" value="${Car?.id}"/>
				
					<label><g:message code="car.price.label" />:</label>${Car.price} kr.<br/>
					<label><g:message code="car.Manufacturer.label" />:</label>${Car.manufacturer}<br/>
					<label><g:message code="car.description.label" />:</label>${Car.description}<br/><br/>
					<label><g:message code="reservation.date.label" />:</label><input type="text" class="datepicker" name="date" /><br><br>
					
					<fieldset class="buttons">
						<g:submitButton name="create" value="${message(code: 'default.button.reserve.label', default: 'Reserve')}" />
					</fieldset>
				</g:form>
			</article>
		</g:each>
	</body>
</html>
