<%@ page import="ordering.Reservation" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="reservation">
		<g:set var="entityName" value="${message(code: 'employeeassignment.label', default: 'Employeeassignment')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<h1><g:message code="store.choose_date.label" /></h1>
		<article class="store">
			<g:form action="reservere">
				<g:hiddenField name="store" value="${params?.id}"/>
			
				<label><g:message code="reservation.date.label" />:</label><input type="text" class="datepicker" name="date" />
				
				<fieldset class="buttons">
					<g:submitButton name="create" value="${message(code: 'default.button.reserve.label', default: 'Reserve')}" />
				</fieldset>
			</g:form>
		</article>
	</body>
</html>
