<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="customReservation">
		<g:set var="entityName" value="${message(code: 'employeeassignment.label', default: 'Employeeassignment')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<h1><g:message code="store.create_reservation.label" /></h1>
		<g:form url="[resource:reservationInstance, action:'reservere']" >
			<label>
				<g:message code="address.zipcode.label" default="Zipcode" />
			</label><br>
			<input type="text" name="zipcode">
			<br>
			<g:hiddenField name="zipcode" value="${params?.zipcode}"/>
			<label>
				<g:message code="address.city.label" default="City" />
			</label><br>
			<input type="text" name="city">
			<br>
			<g:hiddenField name="city" value="${params?.city}"/>
			<br>
			<fieldset class="buttons">
				<g:submitButton name="create" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>
		</g:form>
	</body>
</html>
