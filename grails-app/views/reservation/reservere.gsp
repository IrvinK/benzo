<%@ page import="ordering.Reservation" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="reservation">
		<title><g:message code="store.reservation_final_header"/></title>
	</head>
	<body>
		<g:if test="${flash.error}">
			${flash.error}
		</g:if>
		<g:else>
			<h1><g:message code="store.reservation_final_header" /></h1>
			<g:img dir="images" file="reservationPic.jpg" style="width:500px;"/>
			<p><g:message code="store.reservation_final_message" /></p>
		</g:else>
	</body>
</html>
