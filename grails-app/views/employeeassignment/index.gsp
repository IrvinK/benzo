<%@ page import="administration.Employeeassignment" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="administration">
		<g:set var="entityName" value="${message(code: 'employeeassignment.label', default: 'Employeeassignment')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<h1><g:message code="default.list.label" args="[entityName]" /></h1>
		<article>
			<div id="list-employeeassignment" class="content scaffold-list" role="main">
				<g:if test="${flash.message}">
					<div class="message" role="status">${flash.message}</div>
				</g:if>
				<table>
					<thead>
						<tr>
							<th><g:message code="employeeassignment.employee.label" default="Employee" /></th>
							<th><g:message code="employeeassignment.reservation.label" default="Reservation" /></th>
							<th><g:message code="reservation.date.label" default="Date" /></th>
						</tr>
					</thead>
					<tbody>
					<g:each in="${employeeassignmentInstanceList}" status="i" var="employeeassignmentInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							<td>${fieldValue(bean: employeeassignmentInstance, field: "employee")}</td>
							<td>${fieldValue(bean: employeeassignmentInstance, field: "reservation")}</td>
							<td><g:formatDate format="dd-MM/yyyy" date="${employeeassignmentInstance.reservation.date}"/></td>
						</tr>
					</g:each>
					</tbody>
				</table>
			</div>
		</article>
	</body>
</html>
