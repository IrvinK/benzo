<%@ page import="administration.Employeeassignment" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="administration">
		<g:set var="entityName" value="${message(code: 'employeeassignment.label', default: 'Employeeassignment')}" />
		<g:set var="employee" value="${message(code: 'employee.label', default: 'Employee')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<h1><g:message code="reservationAssignment.reservation_assignment_completed" /></h1>
		<g:message code="reservationAssignment.reservation_assignment_completed_message" />
		<a href="index"><g:message code="default.back.label"  default="Go back"/></a>
	</body>
</html>
