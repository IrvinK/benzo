<%@ page import="administration.Employeeassignment" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="administration">
		<g:set var="entityName" value="${message(code: 'employeeassignment.label', default: 'Employeeassignment')}" />
		<g:set var="employee" value="${message(code: 'employee.label', default: 'Employee')}" />
		<title><g:message code="reservationAssignment.employeelist" args="[entityName]" /></title>
	</head>
	<body>
		<h1><g:message code="reservationAssignment.employeelist" /></h1>
		<article class="reservation">
			<g:form url="[resource:employeeassignmentInstance, action:'assign']" >
				<g:hiddenField name="reservation" value="${params?.id}"/>
				<fieldset class="form">
					<label for="employee">
						<g:message code="reservationAssignment.choose_employee" default="Choose a car" />:
					</label>
					<select name="employee">
						<g:each in="${employees}" var="Employee">
							<option value="${Employee.id}">${Employee.name}</option>
						</g:each>
					</select>
				</fieldset>
				<br>
				<fieldset class="buttons">
					<g:submitButton name="create" value="${message(code: 'default.button.assign.label', default: 'Create')}" />
				</fieldset>
			</g:form>
		</article>
	</body>
</html>
