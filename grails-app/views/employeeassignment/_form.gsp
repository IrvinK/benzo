<%@ page import="administration.Employeeassignment" %>



<div class="fieldcontain ${hasErrors(bean: employeeassignmentInstance, field: 'employee', 'error')} required">
	<label for="employee">
		<g:message code="employeeassignment.employee.label" default="Employee" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="employee" name="employee.id" from="${basic.Employee.list()}" optionKey="id" required="" value="${employeeassignmentInstance?.employee?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: employeeassignmentInstance, field: 'reservation', 'error')} required">
	<label for="reservation">
		<g:message code="employeeassignment.reservation.label" default="Reservation" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="reservation" name="reservation.id" from="${ordering.Reservation.list()}" optionKey="id" required="" value="${employeeassignmentInstance?.reservation?.id}" class="many-to-one"/>

</div>

