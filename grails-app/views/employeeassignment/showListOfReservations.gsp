<%@ page import="administration.Employeeassignment" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="administration">
		<g:set var="entityName" value="${message(code: 'employeeassignment.label', default: 'Employeeassignment')}" />
		<g:set var="employee" value="${message(code: 'employee.label', default: 'Employee')}" />
		<title><g:message code="reservationAssignment.reservationlist" args="[entityName]" /></title>
	</head>
	<body>
		<h1><g:message code="reservationAssignment.reservationlist" /></h1>
		<g:each in="${reservations}" var="Reservation">
			<article class="reservation">
				<label><g:message code="store.label" />:</label>${Reservation.store.name}<br/>
				<label><g:message code="car.label" />:</label>${Reservation.car.manufacturer}<br/>
				<label><g:message code="reservation.date.label" />:</label><g:formatDate format="dd-MM/yyyy" date="${Reservation.date}"/>
				<a href="showListOfEmployees/${Reservation.id}"><g:message code="default.assign.label" args="[employee]"/></a>
			</article>
		</g:each>
	</body>
</html>
