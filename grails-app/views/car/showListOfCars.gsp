<%@ page import="basic.Employee" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="basic">
		<title><g:message code="car.car_list"/></title>
	</head>
	<body>
		<h1><g:message code="car.car_list" /></h1>
		<g:each in="${carList}" var="Car">
			<article class="car">
				
				<label><g:message code="car.Manufacturer.label" />:</label>${Car.manufacturer}<br/><br/>

				<label><g:message code="car.price.label" />:</label>${Car.price}<br/><br/>

				<label><g:message code="car.description.label" />:</label>${Car.description} <br/><br/>

			</article>
		</g:each>
	</body>
</html>
