<%@ page import="basic.Employee" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="basic"/>
		<title><g:message code="car.creation.label" /></title>
	</head>
	<body>
		<g:if test="${flash.error}">
	     	<g:message code="car.creation.uncompleted_message" /></br></br>
	     	<a href="${createLink(uri: '/car/index')}"><g:message code="default.back.label"  default="Go back"/></a>
		</g:if>
		<g:else>
	    	<g:message code="car.creation.completed_message" /></br></br>
			<a href="${createLink(uri: '/car/showListOfCars')}"><g:message code="car.creation.go_back_to_overview_link"  default="Go back"/></a>
		</g:else>
	</body>
</html>
