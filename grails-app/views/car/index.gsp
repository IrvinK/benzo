<%@ page import="basic.Car" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="basic"/>
		<title><g:message code="car.creation.label" /></title>
	</head>
	<body>
		<h1><g:message code="car.creation.label" /></h1>
		<article class="car">
			<g:form url="[resource:carInstance, action:'addCar']">
				<label>
					<g:message code="car.Manufacturer.label" default="Manufacturer" />
				</label><br>

				<select name="manufacturer">			
					<option name="manufacturer" value="${'BMW'}">BMW</option>
					<option name="manufacturer" value="${'FORD'}">Ford</option>
					<option name="manufacturer" value="${'FERRARI'}">Ferrari</option>
					<option name="manufacturer" value="${'OPEL'}">Opel</option>
					<option name="manufacturer" value="${'FIAT'}">Fiat</option>
					<option name="manufacturer" value="${'MERCEDES'}">Mercedes</option>
					<option name="manufacturer" value="${'LAMBORGHINI'}">Lamborghini</option>
					<option name="manufacturer" value="${'ASTONMARTIN'}">Aston Martin</option>
				</select><br>

				<label>
					<g:message code="car.price.label" default="Price" />
				</label><br>
				<input type="text" name="price">
				<br>

				<label>
					<g:message code="car.description.label" default="Description" />
				</label><br>
				<input type="text" name="description">
				<br><br>

				<fieldset class="buttons">
					<g:submitButton name="create" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>	
			</g:form>
		</article>
	</body>
</html>
