<%@ page import="basic.Employee" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="basic"/>
		<title><g:message code="salary.change.label" /></title>
	</head>
	<body>
	    </br><g:if test="${flash.error}">
	     		<g:message code="salary.salary_change_uncompleted" /></br></br>
			</g:if>
			<g:else>
	    		<g:message code="salary.salary_change_completed" /></br></br>
			</g:else>
	    
		<a href="${createLink(uri: '/employee/showListOfEmployees')}"><g:message code="default.back.label"  default="Go back"/></a>
	</body>
</html>
