<%@ page import="basic.Employee" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">	
		<title><g:message code="employee.profile"/></title>
	</head>
	<body>
		<h1><g:message code="employee.profile" /></h1>
		<article class="employee">
			<g:profileButton profileList='${profileList}'/>
		</article>
	</body>
</html>
