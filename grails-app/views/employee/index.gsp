<%@ page import="basic.Employee" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="basic"/>
		
		<title><g:message code="employee.add.label" /></title>
	</head>
	<body>
		<h1><g:message code="employee.add.label" /></h1>
		<article class="employee">
		<g:form url="[resource:employeeInstance, action:'addEmployee']" >	
			<label>
				<g:message code="employee.firstname.label" default="Name" />
			</label><br>
			<input type="text" name="name">
			<g:hiddenField name="name" value="${params?.name}"/>
			<br>

			<g:render template="/templates/addressForm"/>

			<label>
				<g:message code="employee.username" default="Username" />
			</label><br>
			<input type="text" name="username">
			<br>
			<g:hiddenField name="username" value="${params?.username}"/>

			<label>
				<g:message code="employee.password" default="Password" />
			</label><br>
			<input type="text" name="password">
			<br>
			<g:hiddenField name="password" value="${params?.password}"/>
			<br>

			<fieldset class="buttons">
				<g:submitButton name="create" value="${message(code: 'default.button.create.label', default: 'Create')}" />
			</fieldset>	
		</g:form>

		</employee>
		
	</body>
</html>
