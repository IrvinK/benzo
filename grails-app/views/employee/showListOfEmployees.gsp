<%@ page import="basic.Employee" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="basic">
		<title><g:message code="employeeCreation.creation.employeelist"/></title>
	</head>
	<body>
		<h1><g:message code="employeeCreation.creation.employeelist" /></h1>
		<g:each in="${employeeList.sort{it.name}}" var="Employee">
			<article class="employee">
				<label><g:message code="employee.name.label" />:</label>${Employee.name}<br/><br/>
				<label><g:message code="employee.address.label" />:</label>${Employee.address.streetname} </label>${Employee.address.housenumber} <br/>
				<label><g:message/></label>${Employee.address.zipcode} </label>${Employee.address.city}<br/><br/>
				<label><g:message code="employee.type.label" />:</label>${Employee.type}<br/><br/>
				<label><g:message code="employee.username" />:</label>${Employee.user.username}<br/><br/>
				<label><g:message code="employee.salaries.label"/>:</label><br><br>
				<table border="1" style="width:50%">
				<tr>
					<td><label><g:message code="salary.label" /></label></td>
				    <td><label><g:message code="salary.date.label" /></label></td>
				</tr>				
				<g:each in="${Employee.salaries.sort{it.date}}" var="Salary">	
				  <tr>
				    <td>${Salary.salary}kr.</td>
				    <td><g:formatDate format="dd-MM/yyyy HH:mm" date="${Salary.date}"/></td>		
				  </tr>
				</g:each>
				</table><br>
				<g:form url="[resource:employeeInstance, action:'changeSalary']" >
					<g:hiddenField name="employee" value="${Employee.id}"/>
					<label>
						<g:message code="salary.change.label" default="Change salary" />
					</label><br>
					<input type="text" name="salary">
					<g:hiddenField name="salary" value="${params?.salary}"/>
					<br><br>
					<fieldset class="buttons">
						<g:submitButton name="create" value="${message(code: 'default.button.create.label', default: 'Create')}" />
					</fieldset><br>	
				</g:form>
			</article>
		</g:each>
	</body>
</html>
