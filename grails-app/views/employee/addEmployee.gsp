<%@ page import="basic.Employee" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="basic"/>
		<title><g:message code="employee.add.label" /></title>
	</head>
	<body>
			</br><g:if test="${flash.error}">
	     		<g:message code="employeeCreation.employee_creation_uncompleted_message" /></br></br>
			</g:if>
			<g:else>
	    		<g:message code="employeeCreation.employee_creation_completed_message" /></br></br>
			</g:else>
		
		<a href="${createLink(uri: '/basic')}"><g:message code="default.back.label"  default="Go back"/></a>
	</body>
</html>
