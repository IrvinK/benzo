<g:applyLayout name="custom">
	<title><g:layoutTitle default="Benzo"/></title>
	<g:layoutBody/>
	<content tag="menu">
		<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
		<li><g:link action="index"><g:message code="reservationAssignment.assignments_link" /></g:link></li>
		<li><g:link action="showListOfReservations"><g:message code="reservationAssignment.reservationlist_link" /></g:link></li>
	</content>
</g:applyLayout>