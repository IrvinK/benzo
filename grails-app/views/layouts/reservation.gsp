<g:applyLayout name="custom">
	<title><g:layoutTitle default="Benzo"/></title>
	<g:layoutBody/>
	<content tag="menu">
		<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
		<!--<li><g:link controller= "reservation" action="reserver"><g:message code="store.create_reservation.label" /></g:link></li>-->
	</content>
</g:applyLayout>