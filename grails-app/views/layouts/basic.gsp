<g:applyLayout name="custom">
	<title><g:layoutTitle default="Benzo"/></title>
	<g:layoutBody/>
	<content tag="menu">
		<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
		<li><g:link controller="employee" action="index"><g:message code="employee.add.label" /></g:link></li>

		<li><g:link controller="employee" action="showListOfEmployees"><g:message code="employeeCreation.employeelist_link" /></g:link></li>

		<li><g:link controller="car" action="showListOfCars"><g:message code="car.car_list" /></g:link></li>

		<li><g:link controller="car" action="index"><g:message code="car.creation.label" /></g:link></li>
		
		<li><g:link controller="store" action="showListOfAllStores"><g:message code="store.seeAll_link" /></g:link></li>
		<li><g:link controller="store" action="index"><g:message code="store.create_store_link" /></g:link></li>
	</content>
</g:applyLayout>