<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Benzo"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">
  		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700' rel='stylesheet' type='text/css'>
		<asset:stylesheet src="style.css" media="screen"/>
		<asset:stylesheet src="print.css" media="print"/>
		<asset:javascript src="application.js"/>
		<g:layoutHead/>
	</head>
	<body>
		<div id="container">
			<div id="header">
				<nav>
					<ul>
						<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
						<li><g:link controller="employee" action="showProfile"><g:message code="employee.profile" /></g:link></li>
					</ul>
					<div id="languages">
						<sec:ifLoggedIn>
							<g:form controller="logout" action="index" >
								<g:submitButton name="create" value="${message(code: 'default.logout.label', default: 'Logout')}" class="logout" />
							</g:form>
						</sec:ifLoggedIn>
						<a href="?lang=da" class="da" title="Danish"><asset:image src="Denmark-icon.png" alt="Danish"/></a>
						<a href="?lang=en" class="EN" title="English (UK)"><asset:image src="United-Kingdom-icon.png" alt="English (UK)"/></a>
						<a href="?lang=bih" class="BIH" title="Bosnian"><asset:image src="Bosnia-icon.png" alt="English (UK)"/></a>
					</div>
				</nav>
			</div>
			<div id="body">
				<div>
					<g:layoutBody/>
				</div>
			</div>
			<footer>
				&copy; Benzo 2015
			</footer>
		</div>
	</body>
</html>
