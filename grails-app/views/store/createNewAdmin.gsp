<%@ page import="basic.Store" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="basic"/>
		<title><g:message code="store.add.label" /></title>
	</head>
	<body>
			</br><g:if test="${flash.error}">
	     		<g:message code="storeCreation.store_creation_uncompleted_message" /></br></br>
					<a href="${createLink(uri: '/basic')}"><g:message code="default.back.label"  default="Go back"/></a>
			</g:if>
			<g:else>
	    		<g:message code="storeCreation.store_creation_completed_message" /></br></br>
				
				<a href="${createLink(uri: '/store/showListOfAllStores')}"><g:message code="storeCreation.go_back_to_overview_link"  default="Go back"/></a>
			</g:else>		
	</body>
</html>
