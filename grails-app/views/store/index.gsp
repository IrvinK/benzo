<%@ page import="basic.Store" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="basic"/>
		<title><g:message code="store.add.label" /></title>
	</head>
	<body>
		<h1><g:message code="store.add.label" /></h1>
		<article class="store">
			<g:form url="[resource:storeInstance, action:'createNewStore']" >	
				<label>
					<g:message code="store.storename.label" default="Name" />
				</label><br>
				<input type="text" name="storeName">
				<br>

			<label>
				<g:message code="store.type.label" default="Name" />
			</label><br>
			<select name="storeType">
				<option name="storeType" value="${'SALES'}"><g:message code="dk.StoreType.SALES" default="Name" /></option>
				<option name="storeType" value="${'REPAIRS'}"><g:message code="dk.StoreType.REPAIRS" default="Name" /></option>
			</select><br>

				<g:render template="/templates/addressForm"/><br>

				<fieldset class="buttons">
					<g:submitButton name="create" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>	
			</g:form>
		</article>
	</body>
</html>
