<%@ page import="basic.Store" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="basic">
		<title><g:message code="store.storelist" /></title>
	</head>
	<body>
		<h1><g:message code="store.storelist" /></h1>
		<g:each in="${stores.sort{it.name}}" var="Store">
			<article class="employee">
				<label><g:message code="store.name.label" />:</label>${Store.name}<br/><br/>
				<label><g:message code="store.address.label" />:</label>${Store.address.streetname} </label>${Store.address.housenumber} <br/>
				<label><g:message/></label>${Store.address.zipcode} </label>${Store.address.city}<br/><br/>
				<label><g:message code="store.type.label" />:</label>${Store.type}<br/><br/>				
			</article>
		</g:each>
	</body>
</html>
