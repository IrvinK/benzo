<%@ page import="basic.Store" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="basic"/>
		<title><g:message code="employee.add_admin.label"/></title>
	</head>
	<body>
		<h1><g:message code="employee.add_admin.label" /></h1>
		<article class="store">
			</br><g:if test="${flash.error}">
	     		<g:message code="storeCreation.store_creation_uncompleted_message" /></br></br>
					<a href="${createLink(uri: '/store/index')}"><g:message code="default.back.label"  default="Go back"/></a>
			</g:if>
			<g:else>
				<g:form url="[resource:storeInstance, action:'createNewAdmin']" >	
					<label>
						<g:message code="employee.firstname.label" default="Name" />
					</label><br>
					<input type="text" name="name">
					<br>
					
					<g:render template="/templates/addressForm"/>
					
					<label>
						<g:message code="employee.username" default="Username" />
					</label><br>
					<input type="text" name="username">
					<br>

					<label>
						<g:message code="employee.password" default="Password" />
					</label><br>
					<input type="password" name="password">
					<br>
					
					<g:hiddenField name="store" value="${store}"/>

					<br>
					<fieldset class="buttons">
						<g:submitButton name="create" value="${message(code: 'default.button.create.label', default: 'Create')}" />
					</fieldset>	
					
				</g:form>
			</g:else>	
		</store>
	</body>
</html>
