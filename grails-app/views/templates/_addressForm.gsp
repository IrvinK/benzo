<label>
	<g:message code="address.streetname.label" default="Street name" />
</label><br>
<input type="text" name="streetname">
<br>

<label>
	<g:message code="address.housenumber.label" default="House number" />
</label><br>
<input type="text" name="housenumber">
<br>

<label>
	<g:message code="address.zipcode.label" default="Zipcode" />
</label><br>
<input type="text" name="zipcode">
<br>

<label>
	<g:message code="address.city.label" default="City" />
</label><br>
<input type="text" name="city">
<br>
